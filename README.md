# BI-XML Semestralni prace zimni semestr 2019/2020
Nguyen Duc Thang ( nguyed14@fit.cvut.cz )

## Potrebne nastroje

- [saxon](http://saxon.sourceforge.net/)
- [fop](https://xmlgraphics.apache.org/fop/)
- [jing](https://relaxng.org/jclark/jing.html)
- browser

## Vytvoreni source
```xmllint --noent src/merge.xml > src/countries.xml```

## Validace

### DTD
```xmllint --noout --dtdvalid src/validate/schema.dtd src/countries.xml```

### RelaxNG
```xmllint --noout --relaxng src/validate/schema.rnc src/countries.xml```

### Skript
```./scripts/validation.sh argument``` argument DTD nebo RNC

## File structure
```
├── docs                                    # Obsahuje specifikaci
│   ├── BI-XML-PROJEKT-INSTRUKCE.txt
│   ├── BI-XML-PROJEKT-PREZENTACE.txt
│   └── BI-XML-PROJEKT-VYBER-ZADANI.txt
├── README.md                               
├── scripts                                 # Obsahuje vsechny skripta
|   ├── pdf.sh                              # Vygeneruje pdf soubor
|   ├── transform.sh                        # Vygeneruje html stranku pro vsechny stranky
|   └── validation.sh                       # Validace zdroje
└── src                                     # Obsahuje vsechny zdroje
    ├── countries                           # Obsahuje informace a vlajky pro vsechny staty
    │   ├── Czechia-flag.gif                # Vlajka
    │   ├── Czechia-locator-map.gif         # Lokace
    │   ├── Czechia-map.gif                 # Fotka zemi
    │   └── Czechia.xml                     # Informace o dannou zem
    ├── countries.xml                       # Spojene zeme do jednoho filu
    ├── transform                           # Obsahuje specifikaci pro transformaci
    │   ├── countries.xsl
    │   ├── index.xsl
    │   └── pdf.xsl
    └── validate                            # Obsahuje schema na validaci
        ├── schema.dtd
        └── schema.rnc
```
## RNC Validace ukazky

### Kontrola delky
```
element introduction {
        attribute name { xsd:NCName },
        element Background {
          attribute name { xsd:NCName },
          xsd:string { minLength="500" maxLength="2000" }   # OMEZENI DELKY, CHCEME ASPON 500 U KAZDEHO
        }
      }
```
## Vytvoreni PDF

### 
```java -jar saxon/saxon9he.jar src/countries.xml src/transform/pdf.xsl -o:"pdf.fo"```
```fop -fo pdf.fo -pdf countries.pdf 2>/dev/null```
nebo 
```./scripts/pdf.sh```


## Zdroje
- https://www.cia.gov/library/publications/the-world-factbook
- http://zvon.org
- https://stackoverflow.com
- https://gitlab.fit.cvut.cz
- https://getbootstrap.com/docs/4.0/components
- https://xmlgraphics.apache.org/fop/quickstartguide.html
- https://xmlgraphics.apache.org/fop/examples.html
- https://www.kosek.cz/xml/schema/rng.html