#!/bin/bash

DTD="DTD"
RNC="RNC"

if [[ "$1" = "$DTD" ]]; then
    echo "DTD validation."
    xmllint --noout --dtdvalid src/validate/schema.dtd src/countries.xml
    result=$?
    if [ $result -eq 1 ];then
        echo "Test DTD fail."
    else
        echo "Test success."
    fi
elif [[ "$1" = "$RNC" ]]; then
    echo "RNC validation."
    jing -c src/validate/schema.rnc src/countries.xml
    result=$?
    if [ $result -eq 1 ]; then
        echo "Test RNC fail."
    else
        echo "Test success."
    fi
else
    echo "Use DTD or RNC. Not $1. "
fi