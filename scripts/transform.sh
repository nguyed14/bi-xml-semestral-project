 #!/bin/bash
mkdir -p html

# Vygenerujeme index stranku
java -jar saxon/saxon9he.jar src/countries.xml src/transform/index.xsl -o:"html/index.html"

# Vygenerujeme pro vsechny zeme stranku
for country in src/countries/*.xml; do
	echo $country
	country_name=${country%.xml} # odstranime suffix
	country_name=${country_name#*/} # odstranime prefix
	country_name=${country_name#*/}
	echo $country_name
	java -jar saxon/saxon9he.jar $country src/transform/countries.xsl -o:"html/${country_name}.html"
done
